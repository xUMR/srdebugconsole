﻿using System;
using System.Reflection;

public class MemberData
{
    public readonly PropertyInfo PropertyInfo;
    public readonly FieldInfo FieldInfo;

    public readonly string AccessModifiers;
    public readonly string TypeName;
    public readonly string FieldName;

    public readonly bool IsProperty;
    public readonly bool IsReadonlyProperty;
    public readonly bool IsField;
    public readonly bool IsReadonlyField;
    public readonly ValueMemberType Type;

    public MemberData(PropertyInfo propertyInfo)
    {
//        try
//        {
//            // null check (methodInfo == null) for setter throws:
//            //     MissingMethodException: Method not found: 'System.Reflection.MethodInfo.op_Equality'.
//            var n = propertyInfo.GetSetMethod(true).Name;
//        }
//        catch (NullReferenceException e)
//        {
//            IsReadonlyProperty = true;
//        }

        IsReadonlyProperty = !propertyInfo.CanWrite;

        IsProperty = true;
        Type = ValueMemberType.Property;

        PropertyInfo = propertyInfo;
//        AccessModifiers = propertyInfo.GetAccessModifierString();
        AccessModifiers = "property";

        var propertyStr = propertyInfo.ToString();
        var i = propertyStr.LastIndexOf(' ');
        FieldName = propertyStr.Substring(i + 1);
        TypeName = propertyStr.Substring(0, i);
    }

    public MemberData(FieldInfo fieldInfo)
    {
        IsReadonlyField = fieldInfo.IsInitOnly || fieldInfo.IsLiteral;

        IsField = true;
        Type = ValueMemberType.Field;

        FieldInfo = fieldInfo;
        AccessModifiers = fieldInfo.GetAccessModifierString();

        var fieldStr = fieldInfo.ToString();
        var i = fieldStr.LastIndexOf(' ');
        FieldName = fieldStr.Substring(i + 1);
        TypeName = fieldStr.Substring(0, i);
    }

    public void SetValue(object obj, object value)
    {
        switch (Type)
        {
            case ValueMemberType.Field:
                FieldInfo.SetValue(obj, value);
                break;

            case ValueMemberType.Property:
                PropertyInfo.SetValue(obj, value, BindingFlags.Default, null, null,
                    null);
                break;

            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    public object GetValue(object obj)
    {
        switch (Type)
        {
            case ValueMemberType.Field:
                return FieldInfo.GetValue(obj);

            case ValueMemberType.Property:
                return PropertyInfo.GetValue(obj, BindingFlags.Default, null, null,
                    null);

            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    public override string ToString()
    {
        switch (Type)
        {
            case ValueMemberType.Field:
                return string.Format("{0} {1}", AccessModifiers, FieldInfo);

            case ValueMemberType.Property:
                return string.Format("property {0}", PropertyInfo);

            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    public enum ValueMemberType
    {
        Field,
        Property
    }
}
