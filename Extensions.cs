﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

public static class Extensions
{
    public static GameObject[] GetChildren(this GameObject gameObject)
    {
        var transform = gameObject.transform;
        var children = new GameObject[transform.childCount];

        for (var i = 0; i < transform.childCount; i++)
        {
            children[i] = transform.GetChild(i).gameObject;
        }

        return children;
    }

    public static int GetChildCount(this GameObject gameObject)
    {
        return gameObject.transform.childCount;
    }

    public static ObjectNode ToNode(this GameObject gameObject, ObjectNode parent = null, bool fetchChildren = false)
    {
        if (!fetchChildren)
        {
//            return new ObjectNode(gameObject, new ObjectNode[gameObject.GetChildCount()]);
            return new ObjectNode(parent, gameObject, null);
        }

        var children = gameObject.GetChildren();
        var nodes = children.ToNodes(parent);

        return new ObjectNode(parent, gameObject, nodes);
    }

    public static List<ObjectNode> ToNodes(this GameObject[] gameObjects, ObjectNode parent = null, bool fetchChildren = false)
    {
        var list = new List<ObjectNode>(gameObjects.Length);
        foreach (var item in gameObjects)
        {
            list.Add(item.ToNode(parent, fetchChildren));
        }
        return list;
    }

    public static Dictionary<string, object> ToDictionary<T>(this T o)
    {
        var d = new Dictionary<string, object>();

        const BindingFlags bindingFlags = BindingFlags.Public | BindingFlags.NonPublic |
                                          BindingFlags.Instance | BindingFlags.Static;

        foreach (var f in o.GetType().GetFields(bindingFlags))
        {
            d.Add(string.Format("{0} {1}", f.GetAccessModifierString(), f), f.GetValue(o));
        }

        foreach (var p in o.GetType().GetProperties(bindingFlags))
        {
            d.Add(string.Format("property {0}", p), p.GetValue(o, null));
        }

        return d;
    }

    public static Dictionary<MemberData, object> ToDictionary2<T>(this T o)
    {
        var d = new Dictionary<MemberData, object>();

        const BindingFlags bindingFlags = BindingFlags.Public | BindingFlags.NonPublic |
                                          BindingFlags.Instance | BindingFlags.Static;

        foreach (var f in o.GetType().GetFields(bindingFlags)) d.Add(new MemberData(f), f.GetValue(o));

        foreach (var p in o.GetType().GetProperties(bindingFlags)) d.Add(new MemberData(p), p.GetValue(o, null));

        return d;
    }

    public static string GetAccessModifierString(this FieldInfo field)
    {
        var accessModifier = "";

        if (field.IsPublic) accessModifier = "public ";
        else if (field.IsAssembly) accessModifier = "internal ";
        else if (field.IsFamily) accessModifier = "protected ";
        else if (field.IsPrivate) accessModifier = "private ";

        if (field.IsStatic) accessModifier += "static";

        return accessModifier.TrimEnd();
    }

    public static string GetAccessModifierString(this MethodInfo method)
    {
        var accessModifier = "";

        if (method.IsPublic) accessModifier = "public ";
        else if (method.IsAssembly) accessModifier = "internal ";
        else if (method.IsFamily) accessModifier = "protected ";
        else if (method.IsPrivate) accessModifier = "private ";

        if (method.IsStatic) accessModifier += "static";

        return accessModifier.TrimEnd();
    }

    public static string GetAccessModifierString(this PropertyInfo property)
    {
        var accessorModifier = property.GetMethod.GetAccessModifierString();
        var mutatorModifier = property.SetMethod.GetAccessModifierString();

        return string.Format("{0};{1}", accessorModifier, mutatorModifier);
    }
}
