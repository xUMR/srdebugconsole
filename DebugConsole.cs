﻿//using System;
//using System.Text;
//using UnityEngine;
//
//public class DebugConsole : MonoBehaviour
//{
//    private bool _isVisible = true;
//    private static Rect _windowFull = new Rect(60, 60, 620, 530);
//    private static readonly Rect WindowHidden = new Rect(_windowFull.x, _windowFull.y, 0, 0);
//    private readonly Rect _contentRect = new Rect(_windowFull.x - 50, _windowFull.y - 40, _windowFull.width, _windowFull.height - 20);
//    private readonly Rect _btnRect = new Rect(_windowFull.width - 50, 0, 50, 20);
//
//    private Rect _window;
//    private GUIStyle _style;
//
//    private static int _line;
//    private const int MaxLines = 33;
//
//    private static readonly string[] ContentArray = new string[MaxLines];
//    private static string _content;
//    public static string Content
//    {
//        get
//        {
//            return _content;
//        }
//        set
//        {
//            if (_line < MaxLines)
//            {
//                ContentArray[_line++] = value;
//            }
//            else
//            {
//                for (var i = 1; i < MaxLines; i++)
//                {
//                    ContentArray[i - 1] = ContentArray[i];
//                }
//                ContentArray[MaxLines - 1] = value;
//            }
//
//            var sb = new StringBuilder();
//            for (var i = 0; i < MaxLines; i++)
//            {
//                sb.AppendLine(ContentArray[i]);
//            }
//            _content = sb.ToString();
//        }
//    }
//
//    public void Start()
//    {
//        Log("DebugConsole initialized!");
//    }
//
//    public void Update()
//    {
//        if (Input.GetKeyDown(KeyCode.F7))
//        {
//            _isVisible = !_isVisible;
//        }
//    }
//
//    public void OnGUI()
//    {
//        if (_style == null)
//        {
//            _style = GUI.skin.window;
//            _style.alignment = TextAnchor.UpperLeft;
//            _style.fontStyle = FontStyle.Bold;
//        }
//
//        _window = _isVisible ? _windowFull : WindowHidden;
//        _window = GUI.Window(0, _window, WindowFunc, "Log (Press F7 to toggle)", _style);
//    }
//
//    private void WindowFunc(int id)
//    {
//        GUI.Label(_contentRect, Content);
//        if (GUI.Button(_btnRect, "Clear"))
//        {
//            Clear();
//        }
//        GUI.DragWindow();
//    }
//
//    public static void Clear()
//    {
//        for (var i = 0; i < MaxLines; i++)
//        {
//            ContentArray[i] = "";
//        }
//        _content = "";
//        _line = 0;
//    }
//
//    public static void Log(object message, bool singleLine = true)
//    {
//        var str = message.ToString();
//        if (singleLine && str.Length > 80)
//        {
//            str = str.Substring(0, 80);
//        }
//        Content = string.Format("{0:HH:mm:ss}: {1}", DateTime.Now, str);
//        Debug.Log(str);
//    }
//
//    public static void EmptyLine()
//    {
//        _line++;
//    }
//}
