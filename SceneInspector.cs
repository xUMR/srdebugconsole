﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using Object = UnityEngine.Object;

public class SceneInspector : MonoBehaviour
{
    public KeyCode ToggleKey = KeyCode.F6;

    public string[] GameObjectWhiteList = new string[0];

    private Vector2 _objectsScrollPosition;
    private Vector2 _componentsScrollPosition;
    private Vector2 _propertiesScrollPosition;
    private bool _show;

    private const int Margin = 20;

    private Rect _windowRect = new Rect(Margin, Margin, Screen.width - 2 * Margin, Screen.height - 2 * Margin);

    private Scene _scene = SceneManager.GetActiveScene();
    private GameObject[] _rootObjects;
    private List<ObjectNode> _rootNodes;
    private int _objectCount;

    private Object _selectedNode;

    private readonly Component[] _emptyComponentArray = new Component[0];

    public Scene Scene
    {
        get { return _scene; }
        set { _scene = value; }
    }

    public GameObject[] RootObjects
    {
        get { return _rootObjects ?? (_rootObjects = Scene.GetRootGameObjects()); }
        set { _rootObjects = value; }
    }

    public List<ObjectNode> RootNodes
    {
        get { return _rootNodes ?? (_rootNodes = new List<ObjectNode>(RootObjects.ToNodes())); }
        set { _rootNodes = value; }
    }

    private ObjectNode SelectedNode
    {
        get
        {
            foreach (var objectNode in RootNodes)
            {
                if (objectNode.Self != null && objectNode.Self == _selectedNode) return objectNode;
            }
            return null;
        }
        set { _selectedNode = value == null ? null : value.Self; }
    }

    private Component SelectedComponent { get; set; }

    private Object SelectedObject
    {
        get { return SelectedComponent ?? _selectedNode; }
    }

    private Dictionary<MemberData, object> SelectedObjectProperties { get; set; }

    private void Start()
    {
        GUI.skin.font = new Font("Consolas");
    }

    private void OnGUI()
    {
        if (!_show) return;

        _windowRect = GUILayout.Window(0, _windowRect, SceneExplorerWindow,
            "Scene Inspector (Press " + ToggleKey + " to toggle) (" + _scene.name + ") (" + _objectCount + ")");
    }

    private void Update()
    {
        if (Input.GetKeyDown(ToggleKey)) _show = !_show;
        if (Input.GetKey(KeyCode.LeftControl) && Input.GetKeyDown(KeyCode.R)) Refresh();
    }

    private void Refresh()
    {
        Scene = SceneManager.GetActiveScene();
        RootObjects = Scene.GetRootGameObjects();
        RootNodes = RootObjects.ToNodes();
    }

    private void CollapseAll()
    {
        for (var i = RootNodes.Count - 1; i >= 0; i--)
        {
            var node = RootNodes[i];
            if (node.Level > 0)
            {
                RootNodes.RemoveAt(i);
            }

            node.Expanded = false;
        }
    }

    private void Collapse(ObjectNode objectNode, int index = 0)
    {
        for (var i = RootNodes.Count - 1; i >= index; i--)
        {
            var node = RootNodes[i];
            if (node.Parent == objectNode)
            {
                node.Expanded = false;
                Collapse(node, i);
                RootNodes.RemoveAt(i);
                node.Children = null;
            }
        }

        objectNode.Expanded = false;
        objectNode.Children = null;
    }

    private void Expand(ObjectNode node, int index)
    {
        if (node.ChildCount < 1) return;

        node.PopulateChildren();
        RootNodes.InsertRange(index + 1, node.Children);
        node.Expanded = true;
    }

    private void SceneExplorerWindow(int id)
    {
        GUILayout.BeginVertical();
        {
            GUILayout.BeginHorizontal();

            if (GUILayout.Button("Refresh", GUILayout.ExpandWidth(false))) Refresh();
            if (GUILayout.Button("Collapse", GUILayout.ExpandWidth(false))) CollapseAll();

            GUILayout.EndHorizontal();
        }
        {
            GUILayout.BeginHorizontal();

            // game objects
            {
                _objectsScrollPosition =
                    GUILayout.BeginScrollView(_objectsScrollPosition, GUILayout.ExpandWidth(false));

                var objCount = 0;
                for (var i = 0; i < RootNodes.Count; i++)
                {
                    var o = RootNodes[i];
                    if (o != null && o.Self != null)
                    {
                        DisplayNode(o, i);
                        objCount++;
                    }
                }
                _objectCount = objCount;

                GUILayout.EndScrollView();
            }
            // components
            {
                _componentsScrollPosition =
                    GUILayout.BeginScrollView(_componentsScrollPosition, GUILayout.ExpandWidth(false));

                var activeNode = SelectedNode;
                if (activeNode != null)
                {
                    var components = activeNode.Self.GetComponents<Component>() ?? _emptyComponentArray;
                    foreach (var component in components) DisplayComponent(component);
                }

                GUILayout.EndScrollView();
            }
            // properties
            {
                _propertiesScrollPosition =
                    GUILayout.BeginScrollView(_propertiesScrollPosition, GUILayout.ExpandWidth(true));

                if (SelectedObject != null)
                {
                    if (SelectedObjectProperties == null) SelectedObjectProperties = SelectedObject.ToDictionary2();

                    foreach (var pair in SelectedObjectProperties) DisplayMember(pair);
                }

                GUILayout.EndScrollView();
            }

            GUILayout.EndHorizontal();
        }
        GUILayout.EndVertical();
    }

    private void DisplayNode(ObjectNode node, int index)
    {
        GUILayout.BeginHorizontal();
        {
            for (var i = 0; i < node.Level; i++) GUILayout.Space(Margin);

            var active = GUILayout.Toggle(node.Active, "", GUILayout.ExpandWidth(false));
            if (!GameObjectWhiteList.Contains(node.Name)) node.Active = active;

            if (node.ChildCount > 0 && !node.Expanded)
            {
                if (GUILayout.Button("+", GUILayout.ExpandWidth(false))) Expand(node, index);
            }
            else if (node.ChildCount == 0 || node.Expanded)
            {
                if (GUILayout.Button("-", GUILayout.ExpandWidth(false))) Collapse(node);
            }

            GUI.contentColor = node == SelectedNode ? Color.yellow : Color.white;

            if (GUILayout.Button(node.Name, GUILayout.ExpandWidth(false)))
            {
                SelectedNode = SelectedNode == node ? null : node;
                SelectedComponent = null;
                SelectedObjectProperties = null;
            }
            GUI.contentColor = Color.white;
        }
        GUILayout.EndHorizontal();
    }

    private void DisplayComponent(Component c)
    {
        GUI.contentColor = c == SelectedComponent ? Color.yellow : Color.white;

        if (GUILayout.Button(c.GetType().Name, GUILayout.ExpandWidth(false)))
        {
            SelectedComponent = SelectedComponent == c ? null : c;
            SelectedObjectProperties = null;
        }

        GUI.contentColor = Color.white;
    }

    private void DisplayMember(KeyValuePair<MemberData, object> pair)
    {
        var key = pair.Key;

        GUILayout.BeginHorizontal();
        {
            GUILayout.Space(Margin);

            GUI.contentColor = Color.green;
            GUILayout.Label(key.TypeName, GUILayout.ExpandWidth(false));

            GUI.contentColor = Color.cyan;
            GUILayout.Label(key.FieldName, GUILayout.ExpandWidth(false));

            GUI.contentColor = Color.white;

            object value;
            Type valueType;
            if (pair.Value != null)
            {
                value = pair.Value;
                valueType = value.GetType();
            }
            else
            {
                value = "null";
                valueType = typeof(object);
            }

            if (!valueType.IsPrimitive || key.IsReadonlyField || key.IsReadonlyProperty)
            {
                GUILayout.Label(value.ToString(), GUILayout.ExpandWidth(false));
            }
            // ReSharper disable once CheckForReferenceEqualityInstead.1
            // MissingMethodException: Method not found: 'System.Type.op_Equality'.
            else if (valueType.Equals(typeof(bool)))
            {
                var castValue = (bool) value;
                var toggle = GUILayout.Toggle(castValue, "", GUILayout.ExpandWidth(false));
                if (castValue != toggle)
                {
                    key.SetValue(SelectedObject, toggle);
                    SelectedObjectProperties = null;
                }
            }
            else
            {
                var newValue = GUILayout.TextField(value.ToString(), GUILayout.ExpandWidth(false));
                var parsedNewValue = newValue.ParsePrimitive(valueType);
                if (!value.Equals(parsedNewValue))
                {
                    key.SetValue(SelectedObject, parsedNewValue);
                    SelectedObjectProperties = null;
                }
            }
        }
        GUILayout.EndHorizontal();
    }
}
