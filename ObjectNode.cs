﻿using System.Collections.Generic;
using UnityEngine;

public class ObjectNode
{
    public ObjectNode Parent { get; private set; }
    public readonly GameObject Self;
    public readonly string Name;
    public readonly string Tag;
    public int Level;
    public bool Expanded;

    public bool Active
    {
        get { return Self.activeSelf; }
        set { Self.SetActive(value); }
    }

    public int ChildCount
    {
        get { return Self.GetChildCount(); }
    }

    private List<ObjectNode> _children;
    public List<ObjectNode> Children
    {
        get { return _children;  }
        set
        {
            if (value == null && _children != null)
            {
                foreach (var child in _children)
                {
                    child.Children = null;
                }
                _children = null;
//                Parent = null;
                return;
            }
            if (value == null)
            {
                _children = null;
                return;
            }

            foreach (var objectNode in value)
            {
                objectNode.Level = Level + 1;
            }
            _children = value;
        }
    }

    public ObjectNode(ObjectNode parent, GameObject self, List<ObjectNode> children)
    {
        Parent = parent;
        Self = self;
        Name = self.name;
        Tag = self.tag;
        Children = children;
    }

    public void PopulateChildren()
    {
        Children = Self.GetChildren().ToNodes(this);
    }
}
