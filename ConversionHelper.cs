﻿using System;
using System.Diagnostics.CodeAnalysis;

public static class ConversionHelper
{
    public static T ParsePrimitive<T>(this object o)
    {
        return (T)ParsePrimitive(o, typeof(T));
    }

    [SuppressMessage("ReSharper", "CheckForReferenceEqualityInstead.1")]
    public static object ParsePrimitive(this object o, Type type)
    {
        if (!type.IsPrimitive) throw new ArgumentException(type + " is not primitive");

        var str = o.ToString();

        if (type.Equals(typeof(byte))) return byte.Parse(str);
        if (type.Equals(typeof(sbyte))) return sbyte.Parse(str);
        if (type.Equals(typeof(int))) return int.Parse(str);
        if (type.Equals(typeof(uint))) return uint.Parse(str);
        if (type.Equals(typeof(short))) return short.Parse(str);
        if (type.Equals(typeof(ushort))) return ushort.Parse(str);
        if (type.Equals(typeof(long))) return long.Parse(str);
        if (type.Equals(typeof(ulong))) return ulong.Parse(str);
        if (type.Equals(typeof(float))) return float.Parse(str);
        if (type.Equals(typeof(double))) return double.Parse(str);
        if (type.Equals(typeof(char))) return char.Parse(str);
        if (type.Equals(typeof(bool))) return bool.Parse(str);
        if (type.Equals(typeof(decimal))) return decimal.Parse(str);

        throw new ArgumentException();
    }
}
